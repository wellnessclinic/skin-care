Here At Laser at The Maple Leaf we have joined forces with In-DERM Skin Clinic to create the most bespoke Aesthetics clinics in the South East.

We are specialists in Laser Hair Reduction, Vascular Treatments, Cosmetic Blemish Removal and Electrolysis. All our treatments are administered with the highest level of professionalism by our fully trained and registered practitioners.

We offer state of the art technology and partner ourselves with some of the most renowned manufacturers in the industry. We utilise the latest Laser Systems to ensure that you are receiving the best possible treatment for your hair and skin.

Ewa is our Skin and Laser Specialist who has a wide range of experience in all areas of aesthetics.  She has been working as a Laser Specialist here, at The Maple Leaf since 2012.

From 2017 Skin Clinic, owned by Ewa, has joined forces with Laser at The Maple Leaf. Both clinics are here to continue Ewa's personal ethos of providing the best aesthetics in the South East.

Ewa's attention to detail and complete professionalism has provided clinics with many excellent reviews and client base. 

She is also a member of the British Association & Institute of Electrolysis and specialises in preparation for gender reassignment surgery

Laser at The Maple Leaf Recption Area
We are results-driven and only provide treatments that work. We ensure that we give you enough time for your treatments and are sure to fully explain each procedure to you. We guarantee that your expectations are realistic and we make sure you are aware of any risk factors, downtime and costs of treatment.

All our treatments are provided in an exclusive, relaxing, private and professional environment.

Find out more https://www.lasermapleleaf.com